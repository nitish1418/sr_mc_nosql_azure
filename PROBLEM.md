## Problem Statement: Implementing polyglot persistence with NoSQL databases on Azure cloud

The application to be implemented in this case study deals with Insurance policies and interest shown by the users for those policies
It allows insurance providers to add new policies. A user can then search the policies based on the provider name and show interest in policies. 
The application captures the interest shown by users and helps the Insurance providers to obtain the below information from the stored data
    
 - Policies by Provider
 - Total number of users who showed interests on a specified policy that day
 - Users who showed interest in a given policy
 - History of interest data based on provider and policy for future reporting

The application stores data in four types of databases on Azure cloud based on the data requirements
    
 - Cosmos DB Mongo API(Document based): Stores the provider and policy information
 - Azure Cache for Redis(Key-value): Store number of policy name and number of users as key-value pair on that day 
 - Cosmos DB Gremlin API(Graph): Stores Policy and User nodes and the relation between these nodes
 - Cosmos DB Cassandra API(Column-store): Stores policy and user interest data based on time for future reporting 
    
Following is the high level process flow of the application :
    
 - A new provider is added in Document database
 - When a new policy is created, it is stored:
   - As a document in Document-based database
   - As a Graph node
   - As a key in in-memory database with value as a counter of number of users
 - When a user shows interest in a policy
   - A user node is added in graph database and a relation between user and policy is created
   - Counter is incremented in key-value database for the policy
   - A history of the interest data is stored in column-store database
   
   
   ![Architectures Diagram](./NoSQL_Azure.png)

    
Base code for the application is provided in this repository for implementation. Read the **TODO** comments in the code and implement it.

Note:
  - Policy names are unique across providers
  - Only email is stored for a user

 
### The following tasks needs to be completed as part of this casestudy 
  - Login to Azure portal with your credentials
  - Create a database using Cosmos DB with Mongo API
  - Create a database using Cosmos DB with Gremlin API
  - Create key-value cache in Azure Cache for Redis
  - Create a database using Cosmos DB with Cassandra API
  - Create a Keyspace and table in Cassandra. Keyspace name can be obtained from application.properties and table name and its properties can be found from the PolicyInterestHistory Entity class
  - Configure the property values in the application.properties based on the setup done in the above steps
  - Implement the functionality based on the comments provided in base code
  - Respective classes/files contain the **TODO** comments for the code/task to be completed
  - Ensure that all Test cases provided are successful for assignment completion
  - Before running tests, ensure the code implementation is complete and databases are provisioned on Azure

## Instructions
- Take care of whitespace/trailing whitespace
- Do not change the provided class/method names unless instructed
- Ensure your code compiles without any errors/warning/deprecations 
- Check for code smells using SonarLint plugin from STS/IntelliJ IDE
- Follow best practices while coding