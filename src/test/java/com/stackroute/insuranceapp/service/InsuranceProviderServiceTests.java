package com.stackroute.insuranceapp.service;

import com.stackroute.insuranceapp.dao.*;
import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;
import com.stackroute.insuranceapp.model.PolicyInterestHistory;
import com.stackroute.insuranceapp.util.CosmosMongoDbUtility;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InsuranceProviderServiceTests {

    public static final String ICICI_LOMBARD = "ICICI Lombard";
    public static final String LIC = "LIC";
    public static final String HOME_SECURE = "Home Secure";
    public static final String LIFE_SECURE = "Life Secure";
    public static final String USER1_EMAIL = "user1.mail.com";
    public static final String USER2_EMAIL = "user2.mail.com";
    public static final String CAR_SECURE = "Car Secure";

    private static InsuranceProviderServiceImpl providerService;
    private static PolicyUserDaoImpl policyUserDao; //Gremlin
    private static PolicyInterestCountDao interestCountDao; //redis
    private static PolicyInterestHistoryDao interestHistoryDao; //cassandra

    private InsuranceProvider providerOne, providerTwo;
    private InsurancePolicy policyOne, policyTwo, policyThree;

    @BeforeAll
    public static void beforeAll() {
        CosmosMongoDbUtility.getCollection();
        CosmosMongoDbUtility.dropCollection();
        policyUserDao = new PolicyUserDaoImpl();
        policyUserDao.deleteAllNodes();
        interestCountDao = new PolicyInterestCountDaoImpl();
        interestCountDao.deletePolicyCounter(HOME_SECURE);
        interestCountDao.deletePolicyCounter(LIFE_SECURE);
        interestCountDao.deletePolicyCounter(CAR_SECURE);
        interestHistoryDao = PolicyInterestHistoryMapper.getPolicyInterestHistoryDao();
        interestHistoryDao.deleteByProviderAndPolicy(ICICI_LOMBARD, HOME_SECURE);
        interestHistoryDao.deleteByProviderAndPolicy(LIC, LIFE_SECURE);

        providerService = new InsuranceProviderServiceImpl(
                new InsuranceProviderDaoImpl(),
                interestCountDao,
                policyUserDao,
                interestHistoryDao);
    }

    @AfterAll
    public static void afterAll() {
        policyUserDao.closeConnection();
    }

    @BeforeEach
    public void setUp() {
        providerOne = new InsuranceProvider(null, ICICI_LOMBARD, new ArrayList<InsurancePolicy>());
        providerTwo = new InsuranceProvider(null, LIC, new ArrayList<InsurancePolicy>());

        policyOne = new InsurancePolicy(HOME_SECURE, "Home", "http://icilom.com/homesecure");
        policyTwo = new InsurancePolicy(CAR_SECURE, "Car", "http://icilom.com/carsecure");
        policyThree = new InsurancePolicy(LIFE_SECURE, "Life", "http://lic.com/lifesecure");

    }

    @Test
    @Order(1)
    public void givenInsuranceProvidersWhenDoesNotExistThenAddToDb() throws InsuranceProviderExistsException {
        InsuranceProvider providerIcici = providerService.addNewProvider(providerOne);
        InsuranceProvider providerLic = providerService.addNewProvider(providerTwo);
        assertThat(providerIcici, is(notNullValue()));
        assertThat(providerIcici.getProviderName(), equalToIgnoringCase(ICICI_LOMBARD));
        assertThat(providerLic, is(notNullValue()));
        assertThat(providerLic.getProviderName(), equalToIgnoringCase(LIC));
    }

    @Test
    @Order(2)
    public void givenExistingProvidersWhenNewPoliciesAddedThenPolicyStoredInMongoDB() throws InsuranceProviderNotFoundException, PolicyExistsException, PolicyNotFoundException {
        boolean policyOneAdded = providerService.addNewPolicyByProvider(ICICI_LOMBARD, policyOne);
        boolean policyTwoAdded = providerService.addNewPolicyByProvider(ICICI_LOMBARD, policyTwo);
        boolean policyThreeAdded = providerService.addNewPolicyByProvider(LIC, policyThree);
        assertThat(policyOneAdded, is(true));
        assertThat(policyTwoAdded, is(true));
        assertThat(policyThreeAdded, is(true));
        InsuranceProvider providerOne = providerService.getProvider(ICICI_LOMBARD);
        assertThat(providerOne.getPolicies(), hasSize(2));
        assertThat(providerOne.getPolicies(), hasItems(policyOne, policyTwo));
        InsuranceProvider providerTwo = providerService.getProvider(LIC);
        assertThat(providerTwo.getPolicies(), hasSize(1));
        assertThat(providerTwo.getPolicies(), hasItem(policyThree));
    }

    @Test
    @Order(3)
    public void givenNewPoliciesAddedThenPolicyInitializedInRedisAndNeptuneDb() throws InsuranceProviderNotFoundException, PolicyExistsException, PolicyNotFoundException {
        int countForPolicyOne = providerService.getInterestCountForPolicy(policyOne.getPolicyName());
        int countForPolicyTwo = providerService.getInterestCountForPolicy(policyTwo.getPolicyName());
        int countForPolicyThree = providerService.getInterestCountForPolicy(policyThree.getPolicyName());
        assertThat(countForPolicyOne, is(0));
        assertThat(policyUserDao.checkPolicyExists(policyOne.getPolicyName()), is(true));
        assertThat(countForPolicyTwo, is(0));
        assertThat(policyUserDao.checkPolicyExists(policyTwo.getPolicyName()), is(true));
        assertThat(countForPolicyThree, is(0));
        assertThat(policyUserDao.checkPolicyExists(policyThree.getPolicyName()), is(true));
    }


    @Test
    @Order(4)
    public void givenExistingPolicyWhenUserShowsInterestThenPolicyCounterIncrementedInRedis() throws PolicyNotFoundException {
        providerService.saveUsersInterestInPolicy(ICICI_LOMBARD, HOME_SECURE, USER1_EMAIL);
        providerService.saveUsersInterestInPolicy(LIC, LIFE_SECURE, USER1_EMAIL);
        providerService.saveUsersInterestInPolicy(ICICI_LOMBARD, HOME_SECURE, USER2_EMAIL);
        int countForPolicy = providerService.getInterestCountForPolicy(policyOne.getPolicyName());
        assertThat(countForPolicy, is(2));
    }

    @Test
    @Order(5)
    public void givenUserShowsInterestThenPolicyInterestCapturedInNeptuneDB() throws PolicyNotFoundException {
        List<String> policyOneUsers = providerService.getUsersInterestedInPolicy(HOME_SECURE);
        List<String> policyTwoUsers = providerService.getUsersInterestedInPolicy(LIFE_SECURE);
        assertThat(policyOneUsers, hasSize(2));
        assertThat(policyOneUsers, contains(USER1_EMAIL, USER2_EMAIL));
        assertThat(policyTwoUsers, hasSize(1));
        assertThat(policyTwoUsers, contains(USER1_EMAIL));
    }

    @Test
    @Order(6)
    public void givenUserShowsInterestThenPolicyInterestHistoryStoredInCassandraDB() throws PolicyNotFoundException {
        List<PolicyInterestHistory> policyOneInterests = interestHistoryDao.findByProviderAndPolicy(ICICI_LOMBARD, HOME_SECURE).all();
        assertThat(policyOneInterests, hasSize(2));
        assertThat(policyOneInterests, hasItem(hasProperty("userEmail", equalTo(USER2_EMAIL))));
        List<PolicyInterestHistory> policyTwoInterests = interestHistoryDao.findByProviderAndPolicy(LIC, LIFE_SECURE).all();
        assertThat(policyTwoInterests, hasSize(1));
        assertThat(policyTwoInterests, hasItem(hasProperty("userEmail", equalTo(USER1_EMAIL))));
    }

    @Test
    @Order(7)
    public void givenInsuranceProvidersWhenExistsThenThrowException() throws InsuranceProviderExistsException {
        assertThrows(InsuranceProviderExistsException.class,
                () -> providerService.addNewProvider(providerOne));
    }

    @Test
    @Order(8)
    public void givenExistingPolicyWhenAddedThenThrowException() throws InsuranceProviderNotFoundException, PolicyExistsException, PolicyNotFoundException {
        assertThrows(PolicyExistsException.class,
                () -> providerService.addNewPolicyByProvider(ICICI_LOMBARD, policyOne));
    }

    @Test
    @Order(9)
    public void givenNonExistingPolicyWhenUserShowsInterestThenThrowException()  {
        assertThrows(PolicyNotFoundException.class,
                () -> providerService.saveUsersInterestInPolicy(ICICI_LOMBARD, "DummyPolicy", "DummyUser"));
    }

}