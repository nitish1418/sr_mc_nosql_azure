package com.stackroute.insuranceapp.service;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.stackroute.insuranceapp.dao.*;
import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;
import com.stackroute.insuranceapp.model.PolicyInterestHistory;
import com.stackroute.insuranceapp.util.CosmosMongoDbUtility;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InsuranceProviderDaoTests {

    public static final String ICICI_LOMBARD = "ICICI Lombard";
    public static final String LIC = "LIC";
    public static final String HOME_SECURE = "Home Secure";
    public static final String LIFE_SECURE = "Life Secure";
    public static final String USER1_EMAIL = "user1.mail.com";
    public static final String USER2_EMAIL = "user2.mail.com";

    private static InsuranceProviderServiceImpl providerService;
    private static PolicyUserDaoImpl policyUserDao; //Gremlin
    private static PolicyInterestCountDao interestCountDao; //redis
    private static PolicyInterestHistoryDao interestHistoryDao; //cassandra
    private static InsuranceProviderDaoImpl insuranceProviderDao;

    private InsuranceProvider providerOne, providerTwo;
    private InsurancePolicy policyOne, policyTwo, policyThree;

    @BeforeAll
    public static void beforeAll() {
        CosmosMongoDbUtility.getCollection();
        CosmosMongoDbUtility.dropCollection();
        insuranceProviderDao = new InsuranceProviderDaoImpl();
        interestCountDao = new PolicyInterestCountDaoImpl();
        interestCountDao.deletePolicyCounter(HOME_SECURE);
        interestCountDao.deletePolicyCounter(LIFE_SECURE);
        interestHistoryDao = PolicyInterestHistoryMapper.getPolicyInterestHistoryDao();
        interestHistoryDao.deleteByProviderAndPolicy(ICICI_LOMBARD, HOME_SECURE);
        interestHistoryDao.deleteByProviderAndPolicy(ICICI_LOMBARD, LIFE_SECURE);
        interestHistoryDao.deleteByProviderAndPolicy(LIC, LIFE_SECURE);
//
        policyUserDao = new PolicyUserDaoImpl();
        policyUserDao.deleteAllNodes();
//        providerService = new InsuranceProviderServiceImpl(
//                new InsuranceProviderDaoImpl(),
//                interestCountDao,
//                policyUserDao,
//                interestHistoryDao);
    }

    @AfterAll
    public static void afterAll() {
//        policyUserDao.closeConnection();
//        CosmosMongoDbUtility.dropCollection();
    }

    @BeforeEach
    public void setUp() {
        providerOne = new InsuranceProvider(null, ICICI_LOMBARD, new ArrayList<InsurancePolicy>());
        providerTwo = new InsuranceProvider(null, LIC, new ArrayList<InsurancePolicy>());

        policyOne = new InsurancePolicy(HOME_SECURE, "Home", "http://icilom.com/homesecure");
        policyTwo = new InsurancePolicy("Car Secure", "Car", "http://icilom.com/carsecure");
        policyThree = new InsurancePolicy(LIFE_SECURE, "Life", "http://lic.com/lifesecure");

    }

    @Test
    @Order(1)
    public void givenInsuranceProvidersWhenDoesNotExistThenAddToDb() throws InsuranceProviderExistsException {
        InsuranceProvider providerIcici = insuranceProviderDao.insertNewProvider(providerOne);
        InsuranceProvider providerLic = insuranceProviderDao.insertNewProvider(providerTwo);
        assertThat(providerIcici, is(notNullValue()));
        assertThat(providerIcici.getProviderName(), equalToIgnoringCase(ICICI_LOMBARD));
        assertThat(providerLic, is(notNullValue()));
        assertThat(providerLic.getProviderName(), equalToIgnoringCase(LIC));
    }

    @Test
    @Order(2)
    public void givenExistingProvidersWhenNewPoliciesAddedThenPolicyStoredInMongoDB() throws InsuranceProviderNotFoundException, PolicyExistsException, PolicyNotFoundException {
        boolean policyOneAdded = insuranceProviderDao.addPolicyToProvider(ICICI_LOMBARD, policyOne);
        boolean policyTwoAdded = insuranceProviderDao.addPolicyToProvider(ICICI_LOMBARD, policyTwo);
        boolean policyThreeAdded = insuranceProviderDao.addPolicyToProvider(LIC, policyThree);
        assertThat(policyOneAdded, is(true));
        assertThat(policyTwoAdded, is(true));
        assertThat(policyThreeAdded, is(true));
        InsuranceProvider providerOne = insuranceProviderDao.getProvider(ICICI_LOMBARD);
        assertThat(providerOne.getPolicies(), hasSize(2));
        assertThat(providerOne.getPolicies(), hasItems(policyOne, policyTwo));
        InsuranceProvider providerTwo = insuranceProviderDao.getProvider(LIC);
        assertThat(providerTwo.getPolicies(), hasSize(1));
        assertThat(providerTwo.getPolicies(), hasItem(policyThree));
    }


    @Test
    @Order(3)
    public void testRedis() throws InsuranceProviderNotFoundException, PolicyExistsException, PolicyNotFoundException {
        boolean policyOneAdded = interestCountDao.addNewPolicyCounter(HOME_SECURE);
        boolean policytwoAdded = interestCountDao.addNewPolicyCounter(LIFE_SECURE);
        assertThat(policyOneAdded, is(true));
        assertThat(policytwoAdded, is(true));
        assertThat(interestCountDao.getCounterForPolicy(HOME_SECURE), is(0));
        assertThat(interestCountDao.getCounterForPolicy(LIFE_SECURE), is(0));

        interestCountDao.incrementCounterForPolicy(HOME_SECURE);
        interestCountDao.incrementCounterForPolicy(HOME_SECURE);
        assertThat(interestCountDao.getCounterForPolicy(HOME_SECURE), is(2));

        final boolean policyTwoDeleted = interestCountDao.deletePolicyCounter(LIFE_SECURE);
        assertThat(policyTwoDeleted, is(Matchers.is(true)));

    }

    @Test
    @Order(4)
    public void testCassandra() {
        interestHistoryDao.save(new PolicyInterestHistory(ICICI_LOMBARD, HOME_SECURE, UUID.randomUUID(), USER1_EMAIL));
        interestHistoryDao.save(new PolicyInterestHistory(ICICI_LOMBARD, LIFE_SECURE, UUID.randomUUID(), USER1_EMAIL));
        final PagingIterable<PolicyInterestHistory> byProviderAndPolicy = interestHistoryDao.findByProviderAndPolicy(ICICI_LOMBARD, HOME_SECURE);
        final PagingIterable<PolicyInterestHistory> byProviderAndPolicy2 = interestHistoryDao.findByProviderAndPolicy(ICICI_LOMBARD, LIFE_SECURE);
        assertThat(byProviderAndPolicy.getAvailableWithoutFetching(), is(1));
        assertThat(byProviderAndPolicy2.getAvailableWithoutFetching(), is(1));
    }

    @Test
    @Order(5)
    public void testGremlin() throws PolicyExistsException, PolicyNotFoundException {
//        policyUserDao.deleteAllNodes();
        final boolean policyOneAdded = policyUserDao.addNewPolicy(ICICI_LOMBARD, policyOne);
        assertThat(policyOneAdded, is(true));

    }

    @Test
    @Order(6)
    public void testGremlin2() throws PolicyExistsException, PolicyNotFoundException {

        final boolean userInterestAdded = policyUserDao.addUserInterestInPolicy(HOME_SECURE, USER1_EMAIL);
        final boolean userInterestAdded2 = policyUserDao.addUserInterestInPolicy(HOME_SECURE, USER2_EMAIL);
        assertThat(userInterestAdded, is(true));
        assertThat(userInterestAdded2, is(true));
    }

    @Test
    @Order(7)
    public void testGremlin4() throws PolicyNotFoundException {

        final List<String> userInterests = policyUserDao.getUsersInterestedInPolicy(HOME_SECURE);
        assertThat(userInterests.size(), is(2));
    }

}

