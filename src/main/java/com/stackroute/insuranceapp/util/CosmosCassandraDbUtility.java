package com.stackroute.insuranceapp.util;


import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.CqlSessionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * This utility class is used for creating connection to Azure Cosmos DB(Cassandra API)
 */
public class CosmosCassandraDbUtility {

    private static final String SSL = "TLSv1.2";
    private static final String DB_HOST;
    private static final int DB_PORT;
    private static final String DB_USERNAME;
    private static final String DB_PASSWORD;
    private static final String DB_REGION;
    private static final String KEYSTORE_FILE_TYPE;
    private static final String KEYSTORE_FILE_PATH;
    private static final String KEYSTORE_FILE_PASSWORD;


    private static Logger logger = LoggerFactory.getLogger(CosmosCassandraDbUtility.class);

    static {
        DB_HOST = PropertyConfig.getProperty("cosmos.cassandra.host");
        DB_PORT = Integer.parseInt(PropertyConfig.getProperty("cosmos.cassandra.port"));
        DB_USERNAME = PropertyConfig.getProperty("cosmos.cassandra.username");
        DB_PASSWORD = PropertyConfig.getProperty("cosmos.cassandra.password");
        DB_REGION = PropertyConfig.getProperty("cosmos.cassandra.region");
        KEYSTORE_FILE_TYPE = "JKS";
//        KEYSTORE_FILE_PATH = System.getenv("JAVA_HOME") + "/lib/security/cacerts";
        KEYSTORE_FILE_PATH = "/Library/Java/JavaVirtualMachines/jdk-11.0.9.jdk/Contents/Home/lib/security/cacerts";
        KEYSTORE_FILE_PASSWORD = "changeit";

    }

    /* **TODO**
     * The below method should create a CQLSession with Azure CosmosDb(Cassandra API) and return the session
     * Use CQLSession builder to configure the host, port, region, username, password and ssl context
     *  Method getSSLContext() is provided to get the SSLContext
     */
    public CqlSession getSession() {
        CqlSessionBuilder builder = CqlSession.builder().withSslContext(getSSLContext());
        builder.addContactPoint(new InetSocketAddress(DB_HOST, DB_PORT));
        builder.withAuthCredentials(DB_USERNAME, DB_PASSWORD);
        builder.withLocalDatacenter(DB_REGION);
        return builder.build();
    }

    /* This method provides the SSLContext to connect securely to the database
     * using the configurations defined in application.properties file
     */
    private SSLContext getSSLContext() {
        SSLContext sslContext = null;
        try {
            final KeyStore keyStore = KeyStore.getInstance(KEYSTORE_FILE_TYPE);

            File sslKeyStoreFile = new File(KEYSTORE_FILE_PATH);
            try (final InputStream is = new FileInputStream(sslKeyStoreFile)) {
                keyStore.load(is, KEYSTORE_FILE_PASSWORD.toCharArray());

                final KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                kmf.init(keyStore, KEYSTORE_FILE_PASSWORD.toCharArray());
                final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(keyStore);

                sslContext = SSLContext.getInstance(SSL);
                sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
            }
        } catch (NoSuchAlgorithmException | KeyManagementException | CertificateException
                | KeyStoreException | UnrecoverableKeyException | IOException exception) {
            logger.error("SSLContext initialization failed", exception);
            throw new SecurityException("SSL initialization failed");
        }
        return sslContext;
    }
}