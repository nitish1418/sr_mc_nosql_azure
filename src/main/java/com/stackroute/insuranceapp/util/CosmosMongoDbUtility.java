package com.stackroute.insuranceapp.util;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.stackroute.insuranceapp.model.InsuranceProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

/**
 * This class is used for creating a Connection to Azure CosmosDB(Mongo API)
 */
public class CosmosMongoDbUtility {
    private static MongoCollection<InsuranceProvider> mongoCollection;
    private static final String connectionUrl;
    private static final String database;
    private static final String collection;

    static {
        connectionUrl = PropertyConfig.getProperty("cosmos.mongo.connection.url");
        database = PropertyConfig.getProperty("cosmos.mongo.name");
        collection = PropertyConfig.getProperty("cosmos.mongo.collection");
    }

    /**
     * **TODO**
     * This method should return a MongoCollection<InsurancePolicyProvider> based on the
     * configuration read from the application.properties file.
     * Configure the documentdb.connection.url in application properties.
     * Use CodecRegistry for mapping InsurancePolicyProvider class
     */
    public static MongoCollection<InsuranceProvider> getCollection() {

        ConnectionString connectionString = new ConnectionString(connectionUrl);
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .codecRegistry(codecRegistry)
                .build();
        MongoClient mongoClient = MongoClients.create(clientSettings);
        MongoDatabase db = mongoClient.getDatabase(database);
        mongoCollection = db.getCollection(collection, InsuranceProvider.class);
        return mongoCollection;

    }

    /**
     * This method is used for dropping the collection
     */
    public static void dropCollection() {
        mongoCollection.drop();
    }
}
