package com.stackroute.insuranceapp.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.ResultSet;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * This class is used to connect to the AWS Neptune DB
 */
@Slf4j
public class CosmosGremlinDbUtility {
    private Cluster cluster;
    private Client client;

    /**
     * **TODO**
     * Create a connection to the Cosmos DB(Gremlin API) using the Cluster Builder
     * Use the file 'cosmosdb_gremlin_remote.yaml' to configure and build the cluster
     */
    public void createConnection() {

        try {
            cluster = Cluster.build(new File("src/main/resources/cosmosdb_gremlin_remote.yaml")).create();
            client = cluster.connect();
        } catch (FileNotFoundException e) {
            log.error("Couldn't find the configuration file cosmosdb_gremlin_remote.yaml.",e);
        }


    }

    /**
     * **TODO**
     * Use the client connection to submit the query to the GraphDB
     */
    public synchronized ResultSet submitQuery(String query) {
        return client.submit(query);
    }

    /**
     * **TODO**
     * Use cluster object to close the connection
     */
    public void closeConnection() {
        cluster.close();
    }
}