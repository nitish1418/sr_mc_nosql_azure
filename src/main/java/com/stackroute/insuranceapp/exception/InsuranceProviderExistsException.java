package com.stackroute.insuranceapp.exception;

public class InsuranceProviderExistsException extends Exception {
    public InsuranceProviderExistsException(String message) {
        super(message);
    }
}
