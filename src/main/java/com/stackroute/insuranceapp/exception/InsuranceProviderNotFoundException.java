package com.stackroute.insuranceapp.exception;

public class InsuranceProviderNotFoundException extends Exception {
    public InsuranceProviderNotFoundException(String message) {
        super(message);
    }
}
