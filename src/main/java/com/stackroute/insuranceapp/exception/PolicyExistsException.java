package com.stackroute.insuranceapp.exception;

public class PolicyExistsException extends Exception {
    public PolicyExistsException(String message) {
        super(message);
    }
}
