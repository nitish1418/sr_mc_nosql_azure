package com.stackroute.insuranceapp.dao;

import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;

import java.util.List;

/**
 * This interface defines operations to be done on Azure Cosmos DB(Gremlin API) to manage
 * the User and Policy relationship
 */

public interface PolicyUserDao {

    boolean addNewPolicy(String providerName, InsurancePolicy policy) throws PolicyExistsException;

    boolean addUserInterestInPolicy(String policyName, String userEmail) throws PolicyNotFoundException;

    List<String> getUsersInterestedInPolicy(String policyName) throws PolicyNotFoundException;

    boolean checkPolicyExists(String policyName);

    boolean checkUserExists(String userEmail);

    void deleteAllNodes();

    void closeConnection();
}
