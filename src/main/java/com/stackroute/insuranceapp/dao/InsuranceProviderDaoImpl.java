package com.stackroute.insuranceapp.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;
import com.stackroute.insuranceapp.util.CosmosMongoDbUtility;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

/**
 * **TODO**
 * InsuranceProviderDao interface methods should be implemented in this class
 * Log appropriate error/info messages wherever necessary
 */
@Slf4j
public class InsuranceProviderDaoImpl implements InsuranceProviderDao {

    private MongoCollection<InsuranceProvider> mongoCollection;

    /**
     * **TODO**
     * Initialize mongoCollection variable using CosmosMongoDbUtility class
     */
    public InsuranceProviderDaoImpl() {
        this.mongoCollection = CosmosMongoDbUtility.getCollection();

    }

    /**
     * **TODO**
     * This method should add new provider to the Cosmos database
     * If the provider with given providerName already exists in database, then throw InsuranceProviderExistsException
     * For a new provider the policies list should be empty
     * If the input is null/empty, throw IllegalArgumentException
     */
    @Override
    public InsuranceProvider insertNewProvider(InsuranceProvider provider)
            throws InsuranceProviderExistsException {
        if (Optional.ofNullable(provider).isEmpty()) {
            throw new IllegalArgumentException(String.format("Given provider %s is null or empty", provider));
        }
        InsuranceProvider insuranceProvider = mongoCollection.find(Filters.eq("providerName", provider.getProviderName())).first();
        if (insuranceProvider != null) {
            throw new InsuranceProviderExistsException(String.format("Given provider %s already exists", provider));
        }
        mongoCollection.insertOne(provider);

        return provider;
    }

    /**
     * **TODO**
     * This method should get the InsuranceProvider for the given providerName
     * If the provider with given providerName does not exist in database,
     * It should throw InsuranceProviderNotFoundException
     */
    @Override
    public InsuranceProvider getProvider(String providerName)
            throws InsuranceProviderNotFoundException {

        InsuranceProvider insuranceProvider = mongoCollection.find(Filters.eq("providerName", providerName)).first();
        if (Optional.ofNullable(insuranceProvider).isEmpty()) {
            throw new InsuranceProviderNotFoundException(String.format("%s does not exists mongo in database", providerName));
        }

        return insuranceProvider;
    }

    /**
     * **TODO**
     * This method should add new policy for an existing provider
     * If the provider with given providerName is not found, then throw InsuranceProviderNotFoundException
     * If the policy with the given name already exists for the provider, then throw PolicyExistsException
     */
    @Override
    public boolean addPolicyToProvider(String providerName, InsurancePolicy policy)
            throws InsuranceProviderNotFoundException, PolicyExistsException {

        InsuranceProvider insuranceProvider = getProvider(providerName);
        final List<InsurancePolicy> providerPolicies = insuranceProvider.getPolicies();
        int existingPolicies = providerPolicies.size();
        if (providerPolicies.stream().anyMatch(e -> e.equals(policy))) {
            throw new PolicyExistsException(String.format("%s policy already exists in mongo database for provider %s", policy.getPolicyName(), providerName));
        }

        providerPolicies.add(policy);
        insuranceProvider.setPolicies(providerPolicies);
        Document providerDoc = new Document("providerName", providerName);
        FindOneAndReplaceOptions returnDocAfterReplace = new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
        InsuranceProvider updatedProvider = mongoCollection.findOneAndReplace(providerDoc, insuranceProvider, returnDocAfterReplace);
        log.info(String.format("Provider %s updated and document after update is %s", providerName, updatedProvider));
        return updatedProvider.getPolicies().size() == existingPolicies + 1;
    }
}