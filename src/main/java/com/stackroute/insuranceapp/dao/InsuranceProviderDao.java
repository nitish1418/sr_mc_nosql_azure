package com.stackroute.insuranceapp.dao;

import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;

/**
 * This interface defines the methods for database operations on Document database for InsurancePolicy and InsuranceProvider
 */
public interface InsuranceProviderDao {

       InsuranceProvider insertNewProvider(InsuranceProvider provider) throws InsuranceProviderExistsException;

       InsuranceProvider getProvider(String providerName) throws InsuranceProviderNotFoundException;

       boolean addPolicyToProvider(String providerName,InsurancePolicy policy) throws InsuranceProviderNotFoundException, PolicyExistsException;

}
