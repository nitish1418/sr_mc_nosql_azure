package com.stackroute.insuranceapp.dao;

import com.datastax.oss.driver.api.core.CqlIdentifier;
import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.DaoKeyspace;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;
import com.stackroute.insuranceapp.util.CosmosCassandraDbUtility;
import com.stackroute.insuranceapp.util.PropertyConfig;

/**
 * *TODO**
 * This interface is Mapper interface which acts as a factory of DAO objects
 * that will be used to execute requests
 * Apply annotation to make this interface as a Mapper Interface
 */
@Mapper
public interface PolicyInterestHistoryMapper {
    String keyspace = PropertyConfig.getProperty("cosmos.cassandra.keyspace.name");

    CosmosCassandraDbUtility dbUtility = new CosmosCassandraDbUtility();

    /* **TODO**
     * apply annotations to this method and parameter to define
     * it as a factory of PolicyInterestDao object
     */

    @DaoFactory
    PolicyInterestHistoryDao createPolicyInterestHistoryDao(@DaoKeyspace CqlIdentifier keyspace);

    /* **TODO**
     * Create a CQLSession using CosmosCassandraDbUtility class
     * Create a PolicyInterestHistoryDao object using the method of PolicyInterestHistoryMapper interface
     * The DAO should use the keyspace defined in application.properties
     */
    static PolicyInterestHistoryDao getPolicyInterestHistoryDao() {

        final PolicyInterestHistoryMapper policyInterestHistoryMapper = new PolicyInterestHistoryMapperBuilder(dbUtility.getSession()).build();
        return policyInterestHistoryMapper.createPolicyInterestHistoryDao(CqlIdentifier.fromCql(keyspace));
    }
}