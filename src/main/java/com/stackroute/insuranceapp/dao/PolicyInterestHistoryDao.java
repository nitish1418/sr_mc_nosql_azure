package com.stackroute.insuranceapp.dao;

import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.mapper.annotations.*;
import com.stackroute.insuranceapp.model.PolicyInterestHistory;

/**
 * *TODO**
 * This interface provides the query methods for CRUD operations on PolicyInterestHistory entity
 * Apply appropriate annotation to this interface for defining it as a DAO
 */
@Dao
public interface PolicyInterestHistoryDao {

    /* **TODO**
     * This method should define the insert query for saving a PolicyInterestHistory entity
     * Apply annotation to make this method a insert query method
     */
    @Insert
    void save(PolicyInterestHistory interestHistory);

    /* **TODO**
     * This method should define the select query for getting interest history for given
     * provider and policy
     * Apply annotation to make this method a select query method
     */
    @Select
    PagingIterable<PolicyInterestHistory> findByProviderAndPolicy(String providerName, String policyName);

    /* **TODO**
     * This method should define the delete query for deleting all interest history for given
     * provider and policy
     * Apply annotation to make this method a delete query method.
     */
    @Delete(entityClass = PolicyInterestHistory.class)
    void deleteByProviderAndPolicy(String providerName, String policyName);
}
