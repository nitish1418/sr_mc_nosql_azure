package com.stackroute.insuranceapp.dao;

import com.stackroute.insuranceapp.exception.PolicyNotFoundException;

/**
 * This interface defines the operations on the Redis cache for
 * for managing the counter of number of users showing interest in a policy
 */
public interface PolicyInterestCountDao {

    boolean addNewPolicyCounter(String policyName);

    int getCounterForPolicy(String policyName) throws PolicyNotFoundException;

    boolean incrementCounterForPolicy(String policyName) throws PolicyNotFoundException;

    boolean deletePolicyCounter(String policyName);
}
