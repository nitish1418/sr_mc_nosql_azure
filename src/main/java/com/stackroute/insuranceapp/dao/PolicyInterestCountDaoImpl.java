package com.stackroute.insuranceapp.dao;

import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.util.PropertyConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;

/**
 * **TODO**
 * PolicyInterestCountDao interface methods should be implemented in this class
 * Log appropriate error/info messages wherever necessary
 */
@Slf4j
public class PolicyInterestCountDaoImpl implements PolicyInterestCountDao {

    private boolean useSsl = true;
    private String hostName = PropertyConfig.getProperty("redis.host");
    private int port = Integer.parseInt(PropertyConfig.getProperty("redis.port"));
    private String accessKey = PropertyConfig.getProperty("redis.access.key");
    private Jedis jedis;

    public PolicyInterestCountDaoImpl() {
        init(hostName, port, useSsl);
    }

    /* **TODO**
     * Initialize jedis to connect with Azure Cache for Redis
     */
    private void init(String hostName, int port, boolean useSsl) {
        JedisShardInfo shardInfo = new JedisShardInfo(hostName, port, useSsl);
        shardInfo.setPassword(accessKey);
        this.jedis = new Jedis(shardInfo);
    }

    /* **TODO**
     * This method should add a key-value pair in Redis with key as policy name
     * and value initialized to 0. If key with policyname already already exists, return false
     */
    @Override
    public boolean addNewPolicyCounter(String policyName) {
        boolean isPolicyCounterAdded = false;

        if (checkPolicyExists(policyName)) {
            this.jedis.set(policyName, "0");
            isPolicyCounterAdded = true;
        }

        return isPolicyCounterAdded;

    }

    private boolean checkPolicyExists(String policyName) {
        return StringUtils.isEmpty(this.jedis.get(policyName));
    }


    /* **TODO**
     * This method should get the counter value for a given policy name
     * If the given policy name does not exists in Redis, throw PolicyNotFoundException
     */
    @Override
    public int getCounterForPolicy(String policyName) throws PolicyNotFoundException {
        if (checkPolicyExists(policyName)) {
            throw new PolicyNotFoundException(String.format("%s policy not found in redis database", policyName));
        }
        return Integer.parseInt(this.jedis.get(policyName));
    }

    /* **TODO**
     * This method should increment the counter value for a given policy name
     * If the given policy name does not exists in Redis, throw PolicyNotFoundException
     */
    @Override
    public boolean incrementCounterForPolicy(String policyName) throws PolicyNotFoundException {
        if (checkPolicyExists(policyName)) {
            throw new PolicyNotFoundException(String.format("%s policy not found in redis database", policyName));
        }

        Integer policyCounter = Integer.valueOf(this.jedis.get(policyName));
        final String incrementStatus = this.jedis.set(policyName, String.valueOf(++policyCounter));
        log.info(String.format("Policy counter for %s is incremented and value returned is %s", policyName, incrementStatus));
        return incrementStatus.equals("OK");
    }

    /* **TODO**
     * This method should delete the key-value pair for a given policy name
     */
    @Override
    public boolean deletePolicyCounter(String policyName) {
        final Long policyDeleted = this.jedis.del(policyName);
        log.info(String.format("Policy counter for %s is deleted and value returned is %d", policyName, policyDeleted));
        return policyDeleted != 0;
    }
}