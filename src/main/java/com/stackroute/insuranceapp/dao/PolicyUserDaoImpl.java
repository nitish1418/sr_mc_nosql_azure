package com.stackroute.insuranceapp.dao;

import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.util.CosmosGremlinDbUtility;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ResultSet;

import java.util.*;
import java.util.stream.Collectors;

/**
 * **TODO**
 * PolicyUserDao interface methods should be implemented in this class
 * Log appropriate error/info messages wherever necessary
 */
@Slf4j
public class PolicyUserDaoImpl implements PolicyUserDao {
    public static final String POLICY_NAME = "policyName";
    public static final String PROVIDER = "provider";
    public static final String POLICY = "policy";
    public static final String SHOWED_INTEREST = "showed_interest";
    public static final String EMAIL = "email";
    public static final String USER = "user";
    private CosmosGremlinDbUtility dbUtility;

    /**
     * **TODO**
     * Create a connection with CosmosDB using CosmosGremlinDbUtility class
     */
    public PolicyUserDaoImpl() {
        this.dbUtility = new CosmosGremlinDbUtility();
        this.dbUtility.createConnection();
    }

    /**
     * **TODO**
     * This method should add new vertex with label 'policy' to the Graph db with two properties -> provider name and policyname
     * Provide a dummy partition key property 'pk' with value 'pk'
     * If the vertex with given policy name already exists, then throw PolicyExistsException
     * return true, if the vertex was created successfully
     */
    @Override
    public boolean addNewPolicy(String providerName, InsurancePolicy policy) throws PolicyExistsException {
        final String policyName = policy.getPolicyName();
        boolean isVertexAdded = false;

        String addPolicyVertexQ = String.format("g.addV('%s').property('id','%s').property('%s','%s').property('%s','%s').property('pk','pk')", POLICY, policyName, PROVIDER, providerName, POLICY_NAME, policyName);

        if (checkPolicyExists(policyName)) {
            throw new PolicyExistsException(String.format("%s policy already exists in graph database", policyName));
        }
        try {
            final ResultSet addVertexRS = dbUtility.submitQuery(addPolicyVertexQ);
            final Object addVertexStatus = addVertexRS.statusAttributes().get().get("x-ms-status-code");
            log.info(String.format("Policy vertex with name %s is added and status is %s", policyName, addVertexStatus.toString()));
            isVertexAdded = addVertexStatus.toString().equals("200");

        } catch (Exception e) {
            log.error(String.format("Error occurred while adding policy=%s vertex", policyName), e);
        }

        return isVertexAdded;
    }


    /**
     * **TODO**
     * This method should add a relation(edge) 'showed_interest' between `user` and `policy` vertices
     * If user vertex with label named 'user' and property named 'userEmail'does not exist for the given email
     * then a new 'user' vertex should be created
     * If the vertex with given policy name is not found, then throw PolicyNotFoundException
     * return true if the relation has been created successfully
     */
    @Override
    public boolean addUserInterestInPolicy(String policyName, String userEmail) throws PolicyNotFoundException {
        boolean isUserInterestAdded = false;
        final String addUserInterestInPolicyQ = String.format("g.V().hasLabel('%s').has('%s','%s').addE('%s').to(g.V().hasLabel('%s').has('%s','%s'))", USER, EMAIL, userEmail, SHOWED_INTEREST, POLICY, POLICY_NAME, policyName);
        final String addUserVertexQ = String.format("g.addV('%s').property('id','%s').property('%s','%s').property('pk','pk')", USER, userEmail, EMAIL, userEmail);

        if (!checkPolicyExists(policyName)) {
            throw new PolicyNotFoundException(String.format("%s policy not found in graph database", policyName));
        }

        try {
            if (! addUserVertex(userEmail, addUserVertexQ)) return false;

            final ResultSet addUserInterestInPolicyRS = dbUtility.submitQuery(addUserInterestInPolicyQ);
            if (addUserInterestInPolicyRS.all().get().size() == 1) {
                isUserInterestAdded = true;
            }

        } catch (Exception e) {
            log.error(String.format("Error occurred while adding user interest for user=%s iin graph database", userEmail), e);
        }

        return isUserInterestAdded;
    }

    private boolean addUserVertex(String userEmail, String addUserVertexQ) throws InterruptedException, java.util.concurrent.ExecutionException {
        if (!checkUserExists(userEmail)) {
            final ResultSet results = dbUtility.submitQuery(addUserVertexQ);
            if (results.all().get().size() != 1) {
                log.info(String.format("Error occurred while creating the user vertex with name %s ", userEmail));
                return false;
            }
            log.info(String.format("User vertex with name %s is added", userEmail));
        }
        return true;
    }

    /**
     * **TODO**
     * This method should return a List of user who showed interest in the given policy name.
     * If the vertex with given policy name is not found, then throw PolicyNotFoundException
     */
    @Override
    public List<String> getUsersInterestedInPolicy(String policyName) throws PolicyNotFoundException {
        Set<String> users = new HashSet<>();
        final String usersInterestedInPolicyQ = String.format("g.V().hasLabel('%s').has('%s','%s').inE('%s').outV().valueMap()", POLICY, POLICY_NAME, policyName, SHOWED_INTEREST);
        if (!checkPolicyExists(policyName)) {
            throw new PolicyNotFoundException(String.format("%s policy not found in graph database", policyName));
        }
        try {
            final ResultSet usersInterestedInPolicyRS = dbUtility.submitQuery(usersInterestedInPolicyQ);
            List<Result> results = usersInterestedInPolicyRS.all().get();
            users = results.stream().map(e -> ((List<String>)e.get(Map.class).get(EMAIL)).get(0)).collect(Collectors.toSet());
        } catch (Exception e) {
            log.error(String.format("Error occurred while getting user interests in policy=%s vertex", policyName), e);
        }
        return new ArrayList<>(users);
    }

    /**
     * **TODO**
     * This method should check whether a vertex exists for a given policy name
     */
    @Override
    public boolean checkPolicyExists(String policyName) {
        String findPolicyVertexQ = String.format("g.V().hasLabel('%s').has('%s','%s').valueMap()", POLICY, POLICY_NAME, policyName);

        boolean isPolicyExists = false;

        try {
            final ResultSet findPolicyRS = dbUtility.submitQuery(findPolicyVertexQ);

            final int noOfVertexFound = findPolicyRS.all().get().size();

            log.info(String.format("Policy vertex with name %s is being checked and available count is %d", policyName, noOfVertexFound));

            if (noOfVertexFound != 0) {
                isPolicyExists = true;
            }

        } catch (Exception e) {
            log.error(String.format("Error occurred while checking for policy=%s vertex", policyName), e);
        }
        return isPolicyExists;

    }

    /**
     * **TODO**
     * This method should check whether a vertex exists for a given user email
     */
    @Override
    public boolean checkUserExists(String userEmail) {
        String findUserVertexQ = String.format("g.V().hasLabel('%s').has('%s','%s')", USER, EMAIL, userEmail);

        boolean isUserExists = false;

        try {
            final int noOfUserVertexFound = dbUtility.submitQuery(findUserVertexQ).all().get().size();
            log.info(String.format("User vertex with name %s is being checked and available count is %d", userEmail, noOfUserVertexFound));
            if (noOfUserVertexFound != 0) {
                isUserExists = true;
            }

        } catch (Exception e) {
            log.error(String.format("Error occurred while checking for user=%s vertex", userEmail), e);
        }
        return isUserExists;
    }

    /**
     * **TODO**
     * This method should delete all the nodes in the graph
     */
    @Override
    public void deleteAllNodes() {
        String deleteAllVertexQ = "g.V().drop()";
        try {
            dbUtility.submitQuery(deleteAllVertexQ);
        } catch (Exception e) {
            log.error("Error occurred while dropping all vertices", e);
        }
    }

    /**
     * **TODO**
     * This method should close the connection
     */
    public void closeConnection() {
        this.dbUtility.closeConnection();
    }
}