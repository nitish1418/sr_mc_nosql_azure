package com.stackroute.insuranceapp.model;

import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * *TODO**
 * This class represents the PolicyInterestHistory entity to be persisted in Cosmos Cassandra database
 * Use the annotations of Datastax driver mapper module for mapping the class to an entity
 * Apply appropriate annotation to the class to make it an Entity
 * Map the table name as 'policyinterest_by_provider'
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PolicyInterestHistory {
    /* **TODO**
     * Apply appropriate annotation to below fields to achieve the following mapping
     *   providerName   -> partitioning key with column name as 'provider_name'
     *   policyName     -> clustering column with column name as 'policy_name'
     *   timeUuid       -> clustering column with column name as 'time_uuid'
     *   userEmail      -> map with column name 'user_email'
     */
    @PartitionKey
    @CqlName("provider_name")
    private String providerName;

    @ClusteringColumn(1)
    @CqlName("policy_name")
    private String policyName;

    @ClusteringColumn(2)
    @CqlName("time_uuid")
    private UUID timeUuid;

    @CqlName("user_email")
    private String userEmail;
}
