package com.stackroute.insuranceapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InsurancePolicy {

    private String policyName;
    private String policyType;
    private String websiteUrl;
}
