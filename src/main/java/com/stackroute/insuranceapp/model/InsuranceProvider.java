package com.stackroute.insuranceapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InsuranceProvider {
    private ObjectId providerId;
    private String providerName;
    private List<InsurancePolicy> policies;
}
