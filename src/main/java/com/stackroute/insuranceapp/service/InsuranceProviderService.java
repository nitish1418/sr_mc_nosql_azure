package com.stackroute.insuranceapp.service;

import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;

import java.util.List;

/**
 * This interface defines the operations for managing Insurance provider and policies,
 * users showing interest in the policies and History of Users interest in policy.
 */
public interface InsuranceProviderService {
    InsuranceProvider addNewProvider(InsuranceProvider provider) throws InsuranceProviderExistsException;

    boolean addNewPolicyByProvider(String providerName, InsurancePolicy policy) throws InsuranceProviderNotFoundException, PolicyExistsException;

    InsuranceProvider getProvider(String providerName) throws InsuranceProviderNotFoundException;

    int getInterestCountForPolicy(String policyName) throws PolicyNotFoundException;

    boolean saveUsersInterestInPolicy(String providerName, String policyName, String userEmail) throws PolicyNotFoundException;

    List<String> getUsersInterestedInPolicy(String policyName) throws PolicyNotFoundException;
}
