package com.stackroute.insuranceapp.service;

import com.stackroute.insuranceapp.dao.InsuranceProviderDao;
import com.stackroute.insuranceapp.dao.PolicyInterestCountDao;
import com.stackroute.insuranceapp.dao.PolicyInterestHistoryDao;
import com.stackroute.insuranceapp.dao.PolicyUserDao;
import com.stackroute.insuranceapp.exception.InsuranceProviderExistsException;
import com.stackroute.insuranceapp.exception.InsuranceProviderNotFoundException;
import com.stackroute.insuranceapp.exception.PolicyExistsException;
import com.stackroute.insuranceapp.exception.PolicyNotFoundException;
import com.stackroute.insuranceapp.model.InsurancePolicy;
import com.stackroute.insuranceapp.model.InsuranceProvider;
import com.stackroute.insuranceapp.model.PolicyInterestHistory;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

/**
 * TODO
 * Implement the methods of InsuranceProviderService
 */
@Slf4j
public class InsuranceProviderServiceImpl implements InsuranceProviderService {
    private InsuranceProviderDao insuranceProviderDao; //mongo
    private PolicyInterestCountDao interestCountDao; // redis
    private PolicyUserDao policyUserDao; //gremlin
    private PolicyInterestHistoryDao interestHistoryDao; //cassandra

    public InsuranceProviderServiceImpl(InsuranceProviderDao insuranceProviderDao, PolicyInterestCountDao interestCountDao, PolicyUserDao policyUserDao, PolicyInterestHistoryDao interestHistoryDao) {
        this.insuranceProviderDao = insuranceProviderDao;
        this.interestCountDao = interestCountDao;
        this.policyUserDao = policyUserDao;
        this.interestHistoryDao = interestHistoryDao;
    }

    /**
     * TODO
     * Using appropriate dao objects, this method should add a new provider in Azure CosmosDB(Mongo API)
     */
    @Override
    public InsuranceProvider addNewProvider(InsuranceProvider provider) throws InsuranceProviderExistsException {
        return this.insuranceProviderDao.insertNewProvider(provider);
    }

    /**
     * TODO
     * Using appropriate dao objects, this method should get the InsuranceProvider from Azure CosmosDB(Mongo API)
     * given the provider name
     */
    @Override
    public InsuranceProvider getProvider(String providerName) throws InsuranceProviderNotFoundException {
        return this.insuranceProviderDao.getProvider(providerName);
    }

    /**
     * TODO
     * Using appropriate dao objects, this method should add a new policy to an existing provider in Azure CosmosDB(Mongo API)
     * It should also add a new key-value pair for the new policy in Azure cache for Redis
     * It should also add a new vertex for the new policy in Azure CosmosDB(Gremlin API)
     * using appropriate dao objects
     */
    @Override
    public boolean addNewPolicyByProvider(String providerName, InsurancePolicy policy) throws InsuranceProviderNotFoundException, PolicyExistsException {
        boolean policyAdded = insuranceProviderDao.addPolicyToProvider(providerName, policy);
        boolean policyCounterAdded = interestCountDao.addNewPolicyCounter(policy.getPolicyName());
        boolean policyCreated = policyUserDao.addNewPolicy(providerName, policy);
        return policyAdded && policyCounterAdded && policyCreated;
    }

    /**
     * TODO
     * Using appropriate dao objects, this method is used to save the user interest for a policy
     * It should increment the counter for the respective key(policy name) in Azure cache for Redis
     * It should add a relation between the user and the policy vertex for the given useremail and policy name in Azure CosmosDB(Gremlin API)
     * It should also save the users Interest in Azure CosmosDB(Cassandra API) as History
     */
    @Override
    public boolean saveUsersInterestInPolicy(String providerName, String policyName, String userEmail) throws PolicyNotFoundException {

        boolean counterIncremented = this.interestCountDao.incrementCounterForPolicy(policyName);
        boolean interestAdded = this.policyUserDao.addUserInterestInPolicy(policyName, userEmail);
        this.interestHistoryDao.save(new PolicyInterestHistory(providerName, policyName, UUID.randomUUID(), userEmail));
        return counterIncremented && interestAdded;
    }

    /**
     * TODO
     * Using appropriate dao objects, this method should get the interest counter value
     * for the given policy name from Azure cache for Redis
     */
    @Override
    public int getInterestCountForPolicy(String policyName) throws PolicyNotFoundException {
        return this.interestCountDao.getCounterForPolicy(policyName);
    }

    /**
     * TODO
     * Using appropriate dao objects, this method should get the list of user emails
     * who have showed interest for a given policy name from Azure CosmosDB(Gremlin API)
     */
    @Override
    public List<String> getUsersInterestedInPolicy(String policyName) throws PolicyNotFoundException {
        return this.policyUserDao.getUsersInterestedInPolicy(policyName);
    }
}